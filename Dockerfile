FROM arm32v7/openjdk:11-slim
ADD target/dealsfetcher-0.0.1-SNAPSHOT.jar dealsfetcher-0.0.1-SNAPSHOT.jar
EXPOSE 8080
CMD java -jar dealsfetcher-0.0.1-SNAPSHOT.jar